﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight.Threading;
using Hardcodet.Wpf.TaskbarNotification;
using Snotify.Viewmodels;

namespace Snotify
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private TaskbarIcon notifyIcon;

		[STAThread]
		public static void Main(string[] args)
		{
			var application = new App();
			application.InitializeComponent();
			DispatcherHelper.Initialize(); //specify UIThread
			application.Run();
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			//create the notifyicon (it's a resource declared in NotifyIconResources.xaml
			notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
			notifyIcon.DataContext = new NotifyIconViewModel();
		}

		protected override void OnExit(ExitEventArgs e)
		{
			notifyIcon.Visibility = Visibility.Collapsed;
			notifyIcon.Dispose(); //the icon would clean up automatically, but this is cleaner
			base.OnExit(e);
		}
	}
}
