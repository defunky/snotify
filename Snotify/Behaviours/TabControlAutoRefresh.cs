﻿using System.Windows.Controls;
using System.Windows.Interactivity;
using GalaSoft.MvvmLight.Helpers;
using Snotify.Viewmodels.Interfaces;

namespace Snotify.Behaviours
{
	public class TabControlAutoRefresh : Behavior<TabControl>
	{
		protected override void OnAttached()
		{
			AssociatedObject.SelectionChanged += AssociatedObject_SelectionChanged;
			base.OnAttached();
		}

		private void AssociatedObject_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.OriginalSource is TabControl)
			{
				((ITabControl)AssociatedObject.SelectedItem).RefreshStreamers();
			}
		}

		protected override void OnDetaching()
		{
			AssociatedObject.SelectionChanged -= AssociatedObject_SelectionChanged;
			base.OnDetaching();
		}
	}
}