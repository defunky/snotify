﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hardcodet.Wpf.TaskbarNotification;

namespace Snotify.Views
{
	/// <summary>
	/// Interaction logic for Balloon.xaml
	/// </summary>
	public partial class Balloon : UserControl
	{
		public Balloon()
		{
			InitializeComponent();
		}

		public Balloon(string text)
		{
			InitializeComponent();
			BalloonText = text;
		}


		#region BalloonText dependency property

		/// <summary>
		/// Description
		/// </summary>
		public static readonly DependencyProperty BalloonTextProperty =
			DependencyProperty.Register("BalloonText",
				typeof(string),
				typeof(Balloon),
				new FrameworkPropertyMetadata(""));

		/// <summary>
		/// A property wrapper for the <see cref="BalloonTextProperty"/>
		/// dependency property:<br/>
		/// Description
		/// </summary>
		public string BalloonText
		{
			get { return (string)GetValue(BalloonTextProperty); }
			set { SetValue(BalloonTextProperty, value); }
		}

		#endregion

		private void CloseBalloon(object sender, RoutedEventArgs e)
		{
			TaskbarIcon taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
			taskbarIcon.CloseBalloon();
		}
	}
}
