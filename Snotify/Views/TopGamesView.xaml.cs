﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Snotify.Views
{
	/// <summary>
	/// Interaction logic for TopGamesView.xaml
	/// </summary>
	public partial class TopGamesView 
	{
		public TopGamesView()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty ParentUserControlProperty = DependencyProperty.Register(
			"ParentUserControl", typeof(UserControl), typeof(TopGamesView), new PropertyMetadata(default(UserControl)));

		public UserControl ParentUserControl
		{
			get { return (UserControl) GetValue(ParentUserControlProperty); }
			set { SetValue(ParentUserControlProperty, value); }
		}
	}
}
