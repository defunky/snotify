﻿using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Snotify.Views
{
	/// <summary>
	/// Interaction logic for FavouritesView.xaml
	/// </summary>
	public partial class FavouritesView : UserControl
	{
		public FavouritesView()
		{
			InitializeComponent();
		}

		private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
		{
			Regex r = new Regex("^[a-zA-Z0-9]*$");
			string text = ((TextBox) sender).Text;
			if (r.IsMatch(text) && text != string.Empty)
				AddButton.IsEnabled = true;
			else
				AddButton.IsEnabled = false;
		}
	}
}
