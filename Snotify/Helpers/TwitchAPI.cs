﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Snotify.Helpers
{
	public class TwitchAPI
	{
		private static string m_clientid = "aihvbp8dx03b9bvth034vxd5rz327t";

		public static TwitchStream GetUserStream(string name)
		{
			var json = GetJSONData(@"https://api.twitch.tv/kraken/streams/" + name);
			return JsonConvert.DeserializeObject<TwitchStream>(json);
		}

		public static TopGamesContainer GetTopGames()
		{
			var json = GetJSONData(@"https://api.twitch.tv/kraken/games/top?limit=100");

			return JsonConvert.DeserializeObject<TopGamesContainer>(json);
		}

		public static TwitchStreams GetGameStreams(string name)
		{
			string url = $"https://api.twitch.tv/kraken/streams?game={name}&limit=100";
			var json = GetJSONData(url);
			return JsonConvert.DeserializeObject<TwitchStreams>(json);
		}

		private static string GetJSONData(string url)
		{
			string json = string.Empty;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Set("Client-ID", m_clientid);
			//request.AutomaticDecompression = DecompressionMethods.GZip;

			try {
				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream)) {
					json = reader.ReadToEnd();
				}
			}
			catch (Exception e) {
				Console.WriteLine(e.StackTrace);
			}
			//Console.WriteLine(JObject.Parse(json));
			return json;
		}
	}


	#region TwitchStreamer JSON to class

	//Used if single stream is needed
	public class TwitchStream
	{
		[JsonProperty(PropertyName = "stream")]
		public TwitchStreams.TwitchStreamer Stream { get; set; }
	}

	public class TwitchStreams
	{
		[JsonProperty(PropertyName = "streams")]
		public List<TwitchStreamer> Streams { get; set; }

		public class TwitchStreamer
		{
			[JsonProperty(PropertyName = "game")]
			public string Game { get; set; }

			[JsonProperty(PropertyName = "viewers")]
			public string Viewers { get; set; }

			[JsonProperty(PropertyName = "channel")]
			public TwitchChannel Channel { get; set; }

			public class TwitchChannel
			{
				[JsonProperty(PropertyName = "display_name")]
				public string Name { get; set; }

				[JsonProperty(PropertyName = "url")]
				public string URL { get; set; }

				[JsonProperty(PropertyName = "logo")]
				public string Logo { get; set; }

				[JsonProperty(PropertyName = "status")]
				public string Title { get; set; }

				public string Status { get; set; } = "Online";
			}
		}
	}
	#endregion

	#region TopGames JSON to Class
	public class TopGamesContainer
	{
		[JsonProperty(PropertyName = "top")]
		public List<TopGames> TopGamesList { get; set; }

		public class TopGames
		{
			[JsonProperty(PropertyName = "viewers")]
			public string Viewers { get; set; }

			[JsonProperty(PropertyName = "game")]
			public TopGame Game { get; set; }
			public class TopGame
			{
				[JsonProperty(PropertyName = "Name")]
				public string Name { get; set; }

				[JsonProperty(PropertyName = "box")]
				public GameLogo Logo { get; set; }
				public class GameLogo
				{
					[JsonProperty(PropertyName = "medium")]
					public string MediumLogo { get; set; }
				}
			}
		}
		#endregion
	}
}

