﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Hardcodet.Wpf.TaskbarNotification;
using Snotify.Viewmodels.Interfaces;
using Snotify.Views;

namespace Snotify.Viewmodels
{
	public class NotifyIconViewModel : ViewModelBase
	{
		private ControlPanelWindow m_controlPanel;
		private FavouritesViewModel m_favouriteViewModel;

		public NotifyIconViewModel()
		{
			//We want single instance of this to be used throughout the lifetime, for background notifications.
			m_favouriteViewModel = new FavouritesViewModel();
			OpenControlPanel = new RelayCommand(createControlPanel);
			Exit = new RelayCommand(() => Application.Current.Shutdown());
		}

		public void createControlPanel()
		{
			if (m_controlPanel != null) return;
			EventHandler handler = null;
			handler = (s, e) =>
			{
				m_controlPanel.Closed -= handler;
				m_controlPanel = null;
			};

			m_controlPanel = new ControlPanelWindow();
			m_controlPanel.DataContext = new ControlPanelWindowViewModel(
				new ObservableCollection<ITabControl>
				{
						new TransitionViewModel(),
						m_favouriteViewModel,
				});

			m_controlPanel.Closed += handler;
			m_controlPanel.Show();
		}

		public RelayCommand OpenControlPanel { get; private set; }
		public RelayCommand Exit { get; private set; }
	}
}
