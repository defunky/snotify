﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using Snotify.Helpers;
using Snotify.Viewmodels.Interfaces;

namespace Snotify.Viewmodels
{
	class ControlPanelWindowViewModel
	{
		public ControlPanelWindowViewModel(ObservableCollection<ITabControl> tabs)
		{
			Tabs = tabs;
			generateSettings();
		}

		private void generateSettings()
		{
			if (File.Exists("favourites.xml"))
				return;

			using (var writer = XmlWriter.Create("favourites.xml")) {
				writer.WriteStartDocument();
				writer.WriteStartElement("Favourites");
				writer.Close();
			}
		}

		public ObservableCollection<ITabControl> Tabs { get; set; }
	}
}
