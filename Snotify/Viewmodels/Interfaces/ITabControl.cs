﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snotify.Viewmodels.Interfaces
{
	public interface ITabControl
	{
		string TabHeader { get; set; }
		void RefreshStreamers();
	}
}
