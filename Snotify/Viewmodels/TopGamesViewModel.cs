﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Controls;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Snotify.Helpers;
using Snotify.Viewmodels.Interfaces;

namespace Snotify.Viewmodels
{
	public class TopGamesViewModel : ViewModelBase, ITabControl
	{

		public TopGamesViewModel()
		{
			TabHeader = "Top Games";
			ViewStreamers = new RelayCommand<UserControl>(switchToStreamerView);
		}

		private void switchToStreamerView(UserControl transitionViewModel)
		{
			if(SelectedGame != null)
				((TransitionViewModel)transitionViewModel.DataContext).CurrentViewModel = new StreamersViewModel(SelectedGame.Game.Name);
		}

		public string TabHeader { get; set; }
		public TopGamesContainer.TopGames SelectedGame { get; set; }


		public RelayCommand<UserControl> ViewStreamers { get; private set; }

		private ObservableCollection<TopGamesContainer.TopGames> m_topGames;
		public ObservableCollection<TopGamesContainer.TopGames> TopGames
		{
			get { return m_topGames; }
			set { Set(ref m_topGames, value); }
		}

		private bool m_isLoaded;
		public bool IsLoaded
		{
			get { return m_isLoaded; }
			set { Set(ref m_isLoaded, value); }
		}

		public async void RefreshStreamers()
		{
			IsLoaded = false;
			var gameList = await Task.Run(() => TwitchAPI.GetTopGames().TopGamesList);
			TopGames = new ObservableCollection<TopGamesContainer.TopGames>(gameList);
			IsLoaded = true;
		}
	}
}
