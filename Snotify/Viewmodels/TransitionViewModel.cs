﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Snotify.Viewmodels.Interfaces;

namespace Snotify.Viewmodels
{
	public class TransitionViewModel : ViewModelBase, ITabControl
	{
		private ITabControl m_currentViewModel;
		private string m_tabHeader;
		private bool m_enableBack;

		public TransitionViewModel()
		{
			CurrentViewModel = new TopGamesViewModel();
			Back = new RelayCommand(() => CurrentViewModel = new TopGamesViewModel());
		}

		public ITabControl CurrentViewModel
		{
			get { return m_currentViewModel; }
			set
			{
				Set(ref m_currentViewModel, value);
				TabHeader = m_currentViewModel.TabHeader;
				CurrentViewModel.RefreshStreamers();
				RaisePropertyChanged(nameof(EnableBack));
			}
		}
		public void RefreshStreamers()
		{
			CurrentViewModel.RefreshStreamers();
		}

		public string TabHeader
		{
			get { return m_tabHeader; }
			set { Set(ref m_tabHeader, value); }
		}

		public bool EnableBack => !(CurrentViewModel is TopGamesViewModel);

		public RelayCommand Back { get; private set; }



		//public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged
		// = delegate { };
		//private static void NotifyStaticPropertyChanged(string propertyName)
		//{
		//	StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
		//}
	}
}
