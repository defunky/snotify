﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using Hardcodet.Wpf.TaskbarNotification;
using Snotify.Helpers;
using Snotify.Viewmodels.Interfaces;
using Snotify.Views;

namespace Snotify.Viewmodels
{
	public class FavouritesViewModel : ViewModelBase, ITabControl
	{
		private TaskbarIcon m_notifyIcon;
		private Timer m_timer;
		private HashSet<string> m_notified;

		public FavouritesViewModel()
		{

			TabHeader = "Favourites";
			Remove = new RelayCommand<TwitchStreams.TwitchStreamer>(removeFromFavourite);
			Watch = new RelayCommand<TwitchStreams.TwitchStreamer>(watchStream);
			AddToFavourites = new RelayCommand(addToFavourites);

			m_notifyIcon = (TaskbarIcon)Application.Current.FindResource("NotifyIcon");
			m_notified = new HashSet<string>();

			m_timer = new Timer(checkForStreamers, null, 0, (int)TimeSpan.FromSeconds(5).TotalMilliseconds);
		}

		private void checkForStreamers(object sender)
		{
			string notifyNames = string.Empty;
			var favList = loadFavourites();

			foreach (var twitchStreamer in favList) {
				var streamer = twitchStreamer.Channel.Name;
				var status = twitchStreamer.Channel.Status;
				if (!m_notified.Contains(streamer) && status == "Online") {
					if (notifyNames != string.Empty) notifyNames += ", ";
					notifyNames += streamer;
					m_notified.Add(streamer);
				}else if (m_notified.Contains(streamer) && status == "Offline") {
					m_notified.Remove(streamer);
				}
			}

			if (notifyNames != string.Empty) {
				notifyNames += ".";
				DispatcherHelper.UIDispatcher.Invoke(() => m_notifyIcon.ShowCustomBalloon(new Balloon(notifyNames),
					PopupAnimation.Fade, (int?)TimeSpan.FromSeconds(4).TotalMilliseconds));
			}

		}

		private void removeFromFavourite(TwitchStreams.TwitchStreamer streamer)
		{
			XDocument doc = XDocument.Load("favourites.xml");
			var query = from node in doc.Descendants("Streamer")
						let attr = node.Element("Name")
						where attr != null && attr.Value == streamer.Channel.Name
						select node;
			query.ToList().ForEach(x => x.Remove());

			doc.Save("favourites.xml");

			Favourites.Remove(streamer);
		}

		private void addToFavourites()
		{
			XDocument doc = XDocument.Load("favourites.xml");
			XElement favourites = doc.Element("Favourites");
			// ReSharper disable once PossibleNullReferenceException
			favourites.Add(new XElement("Streamer",
					   new XElement("Name", FavouriteName),
					   new XElement("URL", "https://www.twitch.tv/" + FavouriteName),
					   new XElement("Logo")));
			doc.Save("favourites.xml");
			FavouriteName = string.Empty;
			RefreshStreamers();

		}

		private void watchStream(TwitchStreams.TwitchStreamer sender)
		{
			Process.Start(sender.Channel.URL);
		}
		private IOrderedEnumerable<TwitchStreams.TwitchStreamer> loadFavourites()
		{

			XDocument xmlDoc = XDocument.Load("favourites.xml");

			var query = from data in xmlDoc.Descendants("Streamer")
						select new TwitchStreams.TwitchStreamer()
						{
							Channel = new TwitchStreams.TwitchStreamer.TwitchChannel()
							{
								Logo = (string)data.Element("Logo"),
								Name = (string)data.Element("Name"),
								Title = null,
								URL = (string)data.Element("URL"),
								Status = "Offline"
							}
						};

			var favList = query.ToList();
			for (int i = 0; i < favList.Count; ++i) {
				var stream = TwitchAPI.GetUserStream(favList[i].Channel.Name);
				//if no stream information then they're offline
				if (stream?.Stream != null) {
					favList[i] = stream.Stream;
				}
			}

			return favList.OrderByDescending(x => x.Channel.Status);
		}

		public async void RefreshStreamers()
		{
			IsLoaded = false;
			var favouritesList = await Task.Run(() => loadFavourites());
			IsLoaded = true;
			Favourites = new ObservableCollection<TwitchStreams.TwitchStreamer>(favouritesList);
		}

		public string TabHeader { get; set; }
		public RelayCommand<TwitchStreams.TwitchStreamer> Watch { get; private set; }
		public RelayCommand<TwitchStreams.TwitchStreamer> Remove { get; private set; }
		public RelayCommand AddToFavourites { get; private set; }
		public string FavouriteName
		{
			get { return m_favouriteName; }
			set { Set(ref m_favouriteName, value); }
		}

		private bool m_IsLoaded;
		public bool IsLoaded
		{
			get { return m_IsLoaded; }
			set { Set(ref m_IsLoaded, value); }
		}

		private ObservableCollection<TwitchStreams.TwitchStreamer> m_favourites;
		private string m_favouriteName;

		public ObservableCollection<TwitchStreams.TwitchStreamer> Favourites
		{
			get { return m_favourites; }
			set { Set(ref m_favourites, value); }
		}

	}
}
