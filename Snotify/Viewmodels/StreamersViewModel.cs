﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Snotify.Helpers;
using Snotify.Viewmodels.Interfaces;

namespace Snotify.Viewmodels
{
	public class StreamersViewModel : ViewModelBase, ITabControl
	{
		public StreamersViewModel(string gamename)
		{
			TabHeader = gamename;
			Favourite = new RelayCommand<TwitchStreams.TwitchStreamer>(addToFavourite);
			Watch = new RelayCommand<TwitchStreams.TwitchStreamer>(watchStream);
		}

		private void addToFavourite(TwitchStreams.TwitchStreamer sender)
		{
			XDocument doc = XDocument.Load("favourites.xml");
			XElement favourites = doc.Element("Favourites");
			// ReSharper disable once PossibleNullReferenceException
			favourites.Add(new XElement("Streamer",
					   new XElement("Name", sender.Channel.Name),
					   new XElement("URL", sender.Channel.URL),
					   new XElement("Logo", sender.Channel.Logo)));
			doc.Save("favourites.xml");
		}

		public async void RefreshStreamers()
		{
			IsLoaded = false;
			var streams = await Task.Run(() => TwitchAPI.GetGameStreams(TabHeader).Streams);
			IsLoaded = true;
			TwitchStreamers = streams;
		}

		private void watchStream(TwitchStreams.TwitchStreamer sender)
		{
			Process.Start(sender.Channel.URL);
		}

		public RelayCommand<TwitchStreams.TwitchStreamer> Favourite { get; set; }
		public RelayCommand<TwitchStreams.TwitchStreamer> Watch { get; set; }
		public string TabHeader { get; set; }

		private List<TwitchStreams.TwitchStreamer> m_twitchStreamers;
		public List<TwitchStreams.TwitchStreamer> TwitchStreamers
		{
			get { return m_twitchStreamers; }
			set { Set(ref m_twitchStreamers, value); }
		}

		private bool m_IsLoaded;
		public bool IsLoaded
		{
			get { return m_IsLoaded; }
			set { Set(ref m_IsLoaded, value); }
		}


	}
}
